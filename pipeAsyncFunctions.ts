export default function pipeAsync (...fns : any[]) {
    return function (arg) {
        return fns.reduce((p, f) => p.then(f), Promise.resolve(arg));
    }
}
